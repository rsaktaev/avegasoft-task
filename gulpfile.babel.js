import gulp from 'gulp'
import browserSync from 'browser-sync'
import gulpEslint from 'gulp-eslint'
import gulpIf from 'gulp-if'
import gulpBabel from 'gulp-babel'
import gulpSourcemaps from 'gulp-sourcemaps'
import gulpClean from 'gulp-clean'
import gulpSass from 'gulp-sass'
import gulpAutoprefixer from 'gulp-autoprefixer'

const stylesPaths = {
  in: [
    './src/assets/scss/*.scss',
  ],
  out: './src/css/',
}

const jsPaths = {
  in: [
    'node_modules/@babel/polyfill/dist/polyfill.js',
    'node_modules/bootstrap/dist/js/bootstrap.min.js',
    'node_modules/jquery/dist/jquery.min.js',
    'node_modules/popper.js/dist/umd/popper.min.js',
    './src/assets/**/*.js',
  ],
  out: './src/js/',
}

const isFixed = file =>
  file.eslint && file.eslint.fixed

const lint = () =>
  gulp.src('./src/assets/**/*.js')
    .pipe(gulpEslint({ fix: true }))
    .pipe(gulpEslint.format())
    .pipe(gulpIf(isFixed, gulp.dest('./src/assets/**/*.js')))
    .pipe(gulpEslint.failAfterError())

const compileJS = () =>
  gulp.src(jsPaths.in)
    .pipe(gulpSourcemaps.init())
    .pipe(gulpBabel({
      presets: ['@babel/env'],
    }))
    .pipe(gulpSourcemaps.write('./'))
    .pipe(gulp.dest(jsPaths.out))
    .pipe(browserSync.stream())

const compileStyles = () =>
  gulp.src(stylesPaths.in)
    .pipe(gulpSourcemaps.init())
    .pipe(gulpAutoprefixer('last 2 versions', { cascade: false }))
    .pipe(gulpSass({
      outputStyle: 'compressed',
    }))
    .on('error', gulpSass.logError)
    .pipe(gulpSourcemaps.write('./'))
    .pipe(gulp.dest(stylesPaths.out))
    .pipe(browserSync.stream())

const fonts = () =>
  gulp.src('node_modules/@fortawesome/fontawesome-free/webfonts/*')
    .pipe(gulp.dest('./src/webfonts/'))

const clean = () =>
  gulp.src([
    `${stylesPaths.out}/*`,
    `${jsPaths.out}/*`,
    './src/webfonts/*',
  ])
    .pipe(gulpClean({ force: true }))

const server = () => {
  browserSync({
    server: {
      baseDir: './src',
    },
    port: 3000,
  })

  gulp.watch('./**/*.scss', compileStyles)
  gulp.watch('./src/assets/**/*.js', gulp.series([lint, compileJS]))
  gulp.watch('./**/*.{html,css,js}').on('change', browserSync.reload)
}

gulp.task('clean', clean)
gulp.task('build', gulp.series([clean, lint, gulp.parallel([fonts, compileJS, compileStyles])]))
gulp.task('watch', gulp.series(['build', server]))
