$(() => {
  let activeSelector = $('.slider__selector_active') // selector now is active
  let index = activeSelector.data('index') // index of selected image
  const posts = $('.slider-element').length // q of posts
  let interval = null

  const scrollTo = i => {
    index = i
    $('.slider__elements-group').css({
      marginLeft: `calc(-100% * ${i})`, // scroll to selected post
    })

    // now the new button-selector is active
    activeSelector.removeClass('slider__selector_active')
    activeSelector = $('.slider__selector').eq(i).addClass('slider__selector_active')
    clearInterval(interval)
    interval = setInterval(() =>
      scrollTo(index < posts - 1 ? index + 1 : 0), 4000) // reset timer
  }

  $('.slider__nav').click(event => {
    const $target = $(event.target)
    // now scroll to left, right or certain post
    if ($target.hasClass('slider__left')) return scrollTo(index > 0 ? index - 1 : posts - 1)
    if ($target.hasClass('slider__right')) return scrollTo(index < posts - 1 ? index + 1 : 0)
    if ($target.hasClass('slider__selector')) return scrollTo($target.data('index'))
  })

  $('.product-card__quantity-input').change(event => {
    event.preventDefault()
    if (!/^[0-9]*$/.test(event.target.value)) event.preventDefault()
  })

  $('.product-card__quantity-add').click(event => {
    event.preventDefault()
    const quanityInput = $(event.currentTarget).siblings('.product-card__quantity-input')
    quanityInput.val(+quanityInput.val() + 1)
  })

  $('.product-card__quantity-diff').click(event => {
    event.preventDefault()
    const quanityInput = $(event.currentTarget).siblings('.product-card__quantity-input')
    quanityInput.val(quanityInput.val() > 0 ? +quanityInput.val() - 1 : 0)
  })

  interval = setInterval(() =>
    scrollTo(index < posts - 1 ? index + 1 : 0), 4000)
})
