## Run the Project

### Global gulp-cli installation

```
npm i gulp-cli -g
```

### Local dependencies installation

```
npm i
```

### Launching

```
gulp watch
```
