const express = require('express')

const app = express()
app.set('port', process.env.PORT || 3000)
app.use('/', express.static(`${__dirname}/src/`))
app.listen(app.get('port'), () => {
  console.log(`server has started at http://localhost:${app.get('port')}`) // eslint-disable-line no-console
})
